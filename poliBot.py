#!/bin/python3
import telebot
import logging
import os
import psycopg2
import urlparse

#Logger per le attivita' del modulo telebot
logger = telebot.logger
telebot.logger.setLevel(logging.INFO)

#Imposto il token identificativo del bot
bot = telebot.TeleBot("147357756:AAEf-fQfkbYa7hj7gLmuGDb7yLuxEjEShhI")

urlparse.uses_netloc.append("postgres")
url = urlparse.urlparse(os.environ["DATABASE_URL"])

conn = psycopg2.connect(
    database=url.path[1:],
    user=url.username,
    password=url.password,
    host=url.hostname,
    port=url.port
)

cur = conn.cursor()

@bot.message_handler()
def aula(message):
	#Cambio gli spazio in _ per poter gestire anche aule specialsi come:"laboratorio informatico"
	aula = message.text.replace(" ","_")

	try:
		""" Porvo ad aprire il file corrispondente al nome dell'aula, se e' stato inserito 
			un codice d'aula corretto il file esiste e viene aperto, se l'aula non esiste viene 
			eseguita l'except comuncando l'errore all'utente
		"""
		photo = open("photo/"+aula.upper()+"M.png", 'rb')
		bot.send_message(message.chat.id, "Edificio:")
		bot.send_photo(message.chat.id, photo)

		photo = open("photo/"+aula.upper()+"P.png", 'rb')
		bot.send_message(message.chat.id, "Planimetria:")
		bot.send_photo(message.chat.id, photo)

		try:
			
			cur.execute("INSERT INTO stats (aula, output) VALUES (%s, true);",(aula.upper(),))
			conn.commit()

		except:
			pass

	except:
		#Nel caso in cui l'aula ricercata non esiste:
		bot.send_message(message.chat.id, "Errore codice aula")
		try:
			
			cur.execute("INSERT INTO stats (aula, output) VALUES (%s, false);",(aula.upper(),))
			conn.commit()

		except:
			pass
		
#Inizio a ricevere i messaggi, in caso di errore di ricezione il bot non si blocca grazie all'opzione none_stop
bot.polling(none_stop=True)